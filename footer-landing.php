<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package viettel
 */

$options   = get_option( 'themes-option' );
$image_ids = $options['header-logo'];
foreach ( $image_ids as $image_id ) {
	$image = RWMB_Image_Field::file_info( $image_id, array( 'size' => 'full' ) );
}

$title    = rwmb_meta( 'footer-title-setting', ['object_type' => 'setting'], 'themes-option' );
$contact  = rwmb_meta( 'footer-contact-setting', ['object_type' => 'setting'], 'themes-option' );
$link_fb  = rwmb_meta( 'footer-link-fb-setting', ['object_type' => 'setting'], 'themes-option' );
$link_ytb = rwmb_meta( 'footer-link-ytb-setting', ['object_type' => 'setting'], 'themes-option' );


$logo_forms = $options['form-logo'];
foreach ( $logo_forms as $logo_form ) {
	$image_form = RWMB_Image_Field::file_info( $logo_form, array( 'size' => 'full' ) );
}

$title_form     = rwmb_meta( 'form-title', ['object_type' => 'setting'], 'themes-option' );
$shortcode_form = rwmb_meta( 'form-shortcode', ['object_type' => 'setting'], 'themes-option' );
?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="site-info container">
			<div class="footer__wrapper d-flex">
				<div class="footer__left">
					<a href=""><img src="<?php echo $image['url']; ?>"></a>
					<h1><?php echo $title; ?></h1>
					<a class="footer-social" href="<?php echo $link_fb; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/Facebook-footer.png"></a>
					<a class="footer-social" href="<?php echo $link_ytb; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/youtube.png"></a>
				</div>
				<div class="footer__right">
					<?php echo $contact; ?>
				</div>
			</div>
		</div><!-- .site-info -->
		<div class="copyright container">
			<span>Copyright © 2018 VCCHomes. All Right Reserved</span>
			<a href="<?php echo home_url(); ?>/dieu-khoan">Điều khoản</a>
		</div>
	</footer><!-- #colophon -->

	<div class="box-popup">
		<div class="form-popup">
			<img class="form-popup__logo" src="<?php echo $image_form['url']; ?>">
			<h3><?php echo $title_form; ?></h3>
			<?php echo do_shortcode( $shortcode_form ); ?>
		</div>
		<i class="icofont icofont-close"></i>
	</div>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
