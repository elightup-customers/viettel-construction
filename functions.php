<?php
/**
 * viettel functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package viettel
 */

if ( ! function_exists( 'viettel_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function viettel_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on viettel, use a find and replace
		 * to change 'viettel' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'viettel', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'duan-thumbnail', 370, 337, true );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'viettel' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'viettel_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'viettel_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function viettel_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'viettel_content_width', 640 );
}
add_action( 'after_setup_theme', 'viettel_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function viettel_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'viettel' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'viettel' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'viettel_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function viettel_scripts() {
	wp_enqueue_style( 'viettel-theme-slick', get_template_directory_uri() . '/css/slick.css' );
	wp_enqueue_style( 'viettel-icofont', get_template_directory_uri() . '/css/icofont.css', '', '1.0.0' );
	wp_enqueue_style( 'viettel-aos', get_template_directory_uri() . '/css/aos.css', array() );
	wp_enqueue_style( 'viettel-style', get_stylesheet_uri() );

	wp_enqueue_script( 'viettel-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'viettel-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	wp_enqueue_script( 'viettel-slick', get_template_directory_uri() . '/js/slick.min.js', array( 'jquery' ), '1.6', true );
	wp_enqueue_script( 'viettel-aos', get_template_directory_uri() . '/js/aos.js', array( 'jquery' ), '20180510' );
	wp_enqueue_script( 'viettel', get_template_directory_uri() . '/js/script.js', array( 'jquery' ), '1.0', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'viettel_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load read docx file.
 */
require get_template_directory() . '/inc/docfile.php';


/**
 * Scroll to section form after submit gravityform.
 */
add_filter( 'gform_confirmation_anchor_1', '__return_true' );


add_action( 'wp_enqueue_scripts', 'viettel_custom_scripts' );
function viettel_custom_scripts() {
	$object = [
		'ajax_url' => admin_url( 'admin-ajax.php' ),
	];
	wp_enqueue_script( 'viettel-ajax-script', get_stylesheet_directory_uri() . '/js/xem-huong-nha.js', array( 'jquery' ), '', true );
	wp_localize_script( 'viettel-ajax-script', 'ajax_object', $object );
}

function viettel_tracuu() {
	$nam_sinh  = $_POST['namSinh'];
	$gioi_tinh = $_POST['gioiTinh'];
	$nam_xay   = $_POST['namXay'];
	ob_start();
	// $doc      = new DocxConversion( get_template_directory() . '/inc/huong-nha/' . $gioi_tinh . '/' . $nam_sinh . '.docx' );
	// $doc_text = ob_get_clean();
	// $doc_text = $doc->convertToText();

	$huong_nha     = file_get_contents( get_template_directory() . '/inc/huong-nha/' . $gioi_tinh . '/' . $nam_sinh . '.html' );
	$tuoi_xd       = file_get_contents( get_template_directory() . '/inc/tuoi-xd/' . $nam_sinh . '.html' );

	$return = array(
		'huong_nha' => $huong_nha,
		'tuoi_xd'   => $tuoi_xd,
	);
	wp_send_json( $return );
}
add_action( 'wp_ajax_viettel_tracuu', 'viettel_tracuu' );
add_action( 'wp_ajax_nopriv_viettel_tracuu', 'viettel_tracuu' );



function your_prefix_register_post_type() {
	$args = array (
		'label' => esc_html__( 'Báo Giá', 'viettel' ),
		'labels' => array(
			'menu_name' => esc_html__( 'Báo Giá', 'viettel' ),
			'name_admin_bar' => esc_html__( 'Báo Giá', 'viettel' ),
			'add_new' => esc_html__( 'Add new', 'viettel' ),
			'add_new_item' => esc_html__( 'Add new Báo Giá', 'viettel' ),
			'new_item' => esc_html__( 'New Báo Giá', 'viettel' ),
			'edit_item' => esc_html__( 'Edit Báo Giá', 'viettel' ),
			'view_item' => esc_html__( 'View Báo Giá', 'viettel' ),
			'update_item' => esc_html__( 'Update Báo Giá', 'viettel' ),
			'all_items' => esc_html__( 'All Báo Giá', 'viettel' ),
			'search_items' => esc_html__( 'Search Báo Giá', 'viettel' ),
			'parent_item_colon' => esc_html__( 'Parent Báo Giá', 'viettel' ),
			'not_found' => esc_html__( 'No Báo Giá found', 'viettel' ),
			'not_found_in_trash' => esc_html__( 'No Báo Giá found in Trash', 'viettel' ),
			'name' => esc_html__( 'Báo Giá', 'viettel' ),
			'singular_name' => esc_html__( 'Báo Giá', 'viettel' ),
		),
		'public' => true,
		'exclude_from_search' => false,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_nav_menus' => true,
		'show_in_admin_bar' => true,
		'show_in_rest' => true,
		'menu_icon' => 'dashicons-excerpt-view',
		'capability_type' => 'post',
		'hierarchical' => false,
		'has_archive' => true,
		'query_var' => true,
		'can_export' => true,
		'rewrite_no_front' => false,
		'show_in_menu' => true,
		'rest_base' => 'bao-gia',
		'supports' => array(
			'title',
			'editor',
			'thumbnail',
		),
		'rewrite' => true,
	);
	register_post_type( 'bao-gia', $args );
}
add_action( 'init', 'your_prefix_register_post_type' );

add_filter( 'rwmb_meta_boxes', 'your_prefix_register_meta_boxes' );
function your_prefix_register_meta_boxes( $meta_boxes ) {
	$prefix = '';
	$meta_boxes[] = array (
		'title' => esc_html__( 'Giá theo chuyên mục', 'viettel' ),
		'id' => 'gia-theo-chuyen-mc',
		'post_types' => array(
			0 => 'bao-gia',
		),
		'context' => 'normal',
		'priority' => 'high',
		'fields' => array(
			array (
				'id' => $prefix . 'province_id',
				'type' => 'text',
				'name' => esc_html__( 'Mã tỉnh thành', 'viettel' ),
				'std' => 'HNI',
			),
			array (
				'id' => $prefix . 'price_nctb',
				'type' => 'text',
				'name' => esc_html__( 'Giá nhân công xây dựng trung bình', 'viettel' ),
				'std' => 1100000,
				'required' => 1,
			),
			array (
				'id' => $prefix . 'price_tct',
				'type' => 'text',
				'name' => esc_html__( 'Giá gói thi công phần thô', 'viettel' ),
				'std' => 3650000,
				'required' => 1,
			),
			array (
				'id' => $prefix . 'price_htcb',
				'type' => 'text',
				'name' => esc_html__( 'Giá gói hoàn thiện cơ bản', 'viettel' ),
				'std' => 4150000,
				'required' => 1,
			),
			array (
				'id' => $prefix . 'price_ckct',
				'type' => 'group',
				'name' => esc_html__( 'Giá gói chìa khóa trao tay', 'viettel' ),
				'fields' => array(
					array (
						'id' => $prefix . 'price_ckct_tb',
						'type' => 'text',
						'name' => esc_html__( 'Trung bình', 'viettel' ),
						'std' => 5850000,
					),
					array (
						'id' => $prefix . 'price_ckct_k',
						'type' => 'text',
						'name' => esc_html__( 'Khá', 'viettel' ),
						'std' => 6250000,
					),
					array (
						'id' => $prefix . 'price_ckct_cc',
						'type' => 'text',
						'name' => esc_html__( 'Cao cấp', 'viettel' ),
						'std' => 6750000,
					),
				),
				'default_state' => 'expanded',
			),
		),
	);
	return $meta_boxes;
}

function viettel_baogia( $params ) {
	ob_start();

	$args = [
		'post_type' 		=> 'bao-gia',
	];

	if ( isset( $params[ 'tinh-thanh' ] ) ) {
		$args = array_merge(
			$args,
			[
				'meta_key'		=> 'province_id',
				'meta_value'	=> $params[ 'tinh-thanh' ]
			]
		);
	}

	// get posts
	$posts = get_posts( $args );

	$data = [];
	$i = 0;

	foreach ($posts as $post) {
		$data[ $i ][ 'id' ]            = rwmb_meta( 'province_id', $post->ID ) ? rwmb_meta( 'province_id', $post->ID ) : '';
		$data[ $i ][ 'title' ]         = $post->post_title;

		$data[ $i ][ 'price_nctb' ]    = get_post_meta( $post->ID, 'price_nctb' ) ? get_post_meta( $post->ID, 'price_nctb' )[0] : 0;
		$data[ $i ][ 'price_tct' ]     = get_post_meta( $post->ID, 'price_tct' ) ? get_post_meta( $post->ID, 'price_tct' )[0] : 0;
		$data[ $i ][ 'price_htcb' ]    = get_post_meta( $post->ID, 'price_htcb' ) ? get_post_meta( $post->ID, 'price_htcb' )[0] : 0;

		$price_ckct = get_post_meta( $post->ID, 'price_ckct' );
		$data[ $i ][ 'price_ckct' ] = $price_ckct[0];

		$i++;
	}

	ob_end_clean();
	return $data;
}

// register the endpoint
add_action( 'rest_api_init', function () {
	register_rest_route( 'bao-gia/v1', 'newest/', [
		'methods' => 'GET',
		'callback' => 'viettel_baogia',
	] );
} );


// Register a theme options page
add_filter( 'mb_settings_pages', function ( $settings_pages ) {
	$settings_pages[] = array(
		'id'          => 'themes-option',
		'option_name' => 'themes-option',
		'menu_title'  => 'Tùy chỉnh',
		'icon_url'    => 'dashicons-edit',
		'style'       => 'no-boxes',
		'columns'     => 1,
		'tabs'        => array(
			'header'     => 'Header Settings',
			'footer'     => 'Footer Settings',
			'popup_form' => 'Form popup',
		),
	);
	return $settings_pages;
} );


// Register meta boxes and fields for settings page
add_filter( 'rwmb_meta_boxes', function ( $meta_boxes ) {
	$meta_boxes[] = array(
		'id'             => 'header',
		'title'          => 'Header',
		'settings_pages' => 'themes-option',
		'tab'            => 'header',
		'fields' => array(
			array(
				'name' => esc_html__( 'Logo', 'viettel' ),
				'id'   => 'header-logo',
				'type' => 'image_advanced',
				'max_file_uploads' => 1,
			),
			array(
				'id' => 'header-phone-setting',
				'type' => 'textarea',
				'name' => esc_html__( 'Phone', 'viettel' ),
			),
			array(
				'id' => 'header-email-setting',
				'type' => 'textarea',
				'name' => esc_html__( 'Email', 'viettel' ),
			),
			array(
				'id' => 'footer-link-fb-setting',
				'type' => 'text',
				'name' => esc_html__( 'Link Facebook', 'viettel' ),
			),
			array(
				'id' => 'footer-link-ytb-setting',
				'type' => 'text',
				'name' => esc_html__( 'Link Youtube', 'viettel' ),
			),
			array(
				'id' => 'header-social-setting',
				'type' => 'textarea',
				'name' => esc_html__( 'Social', 'viettel' ),
			),
		),
	);
	$meta_boxes[] = array(
		'id'             => 'footer',
		'title'          => 'Footer',
		'settings_pages' => 'themes-option',
		'tab'            => 'footer',
		'fields' => array(
			array(
				'id' => 'footer-title-setting',
				'type' => 'text',
				'name' => esc_html__( 'Tiêu đề', 'viettel' ),
			),
			array(
				'id' => 'footer-contact-setting',
				'type' => 'textarea',
				'name' => esc_html__( 'Text liên hệ', 'viettel' ),
			),
		),
	);
	$meta_boxes[] = array(
		'id'             => 'popup_form',
		'title'          => 'Form popup',
		'settings_pages' => 'themes-option',
		'tab'            => 'popup_form',
		'fields' => array(
			array(
				'name' => esc_html__( 'Logo form', 'viettel' ),
				'id'   => 'form-logo',
				'type' => 'image_advanced',
				'max_file_uploads' => 1,
			),
			array(
				'name' => esc_html__( 'Background form', 'viettel' ),
				'id'   => 'form-background',
				'type' => 'image_advanced',
				'max_file_uploads' => 1,
			),
			array(
				'id' => 'form-title',
				'type' => 'textarea',
				'name' => esc_html__( 'Title Form Popup', 'viettel' ),
			),
			array(
				'id' => 'form-shortcode',
				'type' => 'text',
				'name' => esc_html__( 'Form shortcode', 'viettel' ),
			),
		),
	);
	return $meta_boxes;
} );
