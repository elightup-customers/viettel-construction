<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package viettel
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-160838830-1"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-160838830-1');
	</script>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-KH5DKZ8');</script>
	<!-- End Google Tag Manager -->
</head>

<body <?php body_class(); ?>>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KH5DKZ8"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'viettel' ); ?></a>

<?php
// $footer_logos = rwmb_meta( 'footer-logo', array( 'size' => 'full' ) );
$logo      = get_option( 'themes-option' );
$image_ids = $logo['header-logo'];
foreach ( $image_ids as $image_id ) {
	$image = RWMB_Image_Field::file_info( $image_id, array( 'size' => 'full' ) );
}

$phone        = rwmb_meta( 'header-phone-setting', ['object_type' => 'setting'], 'themes-option' );
$email        = rwmb_meta( 'header-email-setting', ['object_type' => 'setting'], 'themes-option' );
$link_fb      = rwmb_meta( 'footer-link-fb-setting', ['object_type' => 'setting'], 'themes-option' );
$link_ytb     = rwmb_meta( 'footer-link-ytb-setting', ['object_type' => 'setting'], 'themes-option' );
$title_social = rwmb_meta( 'header-social-setting', ['object_type' => 'setting'], 'themes-option' );
?>

	<header id="masthead" class="site-header">
		<div class="topbar d-flex">
			<div class="site-branding">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo $image['url']; ?>"></a>
			</div><!-- .site-branding -->
			<div class="header-contact d-flex">
				<div class="header-contact__phone">
					<img src="<?php echo get_template_directory_uri(); ?>/images/phone.png">
					<span><?php echo $phone; ?></span>
				</div>
				<div class="header-contact__email">
					<img src="<?php echo get_template_directory_uri(); ?>/images/mail.png">
					<span><?php echo $email; ?></span>
				</div>
				<div class="header-contact__social">
					<span><b><?php echo $title_social; ?></b></span>
					<a href="<?php echo $link_fb; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/facebook.png"></a>
					<a href="<?php echo $link_ytb; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/youtube.png"></a>
				</div>
			</div>
		</div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
