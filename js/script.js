jQuery( function ( $ ) {
	var $body = $( 'body' );
	var $this = $( this );

	/**
	 * Aos.
	 */
	var aos = function () {
		AOS.init({
			offset: 10,
			duration: 1000,
			once: true,
		})
	}

	var slickSlider = function () {
		$( '.banner-top' ).slick( {
			slidesToShow: 1,
			slidesToScroll: 1,
			nextArrow: '<button type="button" class="slick-next slick-nav"><img src="/wp-content/themes/viettel/images/slick-next.png"></button>',
			prevArrow: '<button type="button" class="slick-prev slick-nav"><img src="/wp-content/themes/viettel/images/slick-prev.png"></button>',
		} );

		$( '.slider-nav' ).slick( {
			slidesToShow: 5,
			slidesToScroll: 1,
			arrows: false,
			asNavFor: '.slider-single',
			responsive: [{
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				}
			}]
		} ).on( 'setPosition', function (event, slick) {
			slick.$slides.css( 'height', slick.$slideTrack.height() + 'px' );
		} );
		$( '.slider-single' ).slick( {
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			dots: true,
			asNavFor: '.slider-nav'
		} );
		$( '.slider-nav__item' ).on( 'click',  function() {
			$index = $( this ).data( 'slick-index' );
			$( '.slider-single' ).slick( 'slickGoTo', $index );
		} );

		$( '.featured__item--right' ).slick( {
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: true,
			// infinite: false,
			fade: true,
			cssEase: 'ease',
			nextArrow: '<button type="button" class="slick-next slick-nav"><img src="/wp-content/themes/viettel/images/slick-next.png"></button>',
			prevArrow: '<button type="button" class="slick-prev slick-nav"><img src="/wp-content/themes/viettel/images/slick-prev.png"></button>',
		} );

		$( '.projects__wrapper' ).slick( {
			slidesToShow: 3,
			slidesToScroll: 1,
			// infinite: false,
			nextArrow: '<button type="button" class="slick-next slick-nav"><img src="/wp-content/themes/viettel/images/slick-next.png"></button>',
			prevArrow: '<button type="button" class="slick-prev slick-nav"><img src="/wp-content/themes/viettel/images/slick-prev.png"></button>',
			responsive: [
				{
					breakpoint: 991,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 1
					}
				},
				{
					breakpoint: 600,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				}
			]
		} );

		$( '.single__recent-wrapper' ).slick( {
			slidesToShow: 6,
			slidesToScroll: 1,
			// infinite: false,
			nextArrow: '<button type="button" class="slick-next slick-nav"><img src="/wp-content/themes/viettel/images/slick-next.png"></button>',
			prevArrow: '<button type="button" class="slick-prev slick-nav"><img src="/wp-content/themes/viettel/images/slick-prev.png"></button>',
			responsive: [
				{
					breakpoint: 1281,
					settings: {
						slidesToShow: 4,
						slidesToScroll: 1
					}
				},
				{
					breakpoint: 991,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 1
					}
				},
				{
					breakpoint: 600,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				}
			]
		} );
	}


	var extendTinhGia = function () {
		$( '.extend' ).on( 'click', function() {
			$( '.item:nth-child(3)' ).toggle(300);
			if ( $( this ).text() == 'Thu gọn' ) {
				$( this ).text( 'Mở rộng' );
			} else {
				$( this ).text( 'Thu gọn' );
			}
			if ( detectMob() ) {
				$( '.input.hide-on-mobile' ).toggle(300);
			}
		} );
	}

	/**
	 * Counter Statistic.
	 */
	var countStatistic = function(argument) {
		var a = 0;
		$(window).scroll(function() {
			var oTop = $( '.thongso' ).offset().top - window.innerHeight;
			if (a == 0 && $(window).scrollTop() > oTop) {
				$( '.number' ).each(function() {
					$(this).prop( 'Counter', 0 ).animate({
						Counter: $(this).text()
					}, {
						duration: 2000,
						easing: 'swing',
						step: function( now ) {
							$(this).text( Math.ceil( now ) );
						},
					});
				});
				a = 1;
			}

		});
	}

	var calculatorMarginService = function() {
		$banner_item_height = $( '.item__content' ).outerHeight();
		$( '.page-template-landing .service' ).css( {
			'margin-top':  - $banner_item_height,
			'padding-top' : $banner_item_height - 50
		} );
	}

	function formatPrice( price ) {
		return price.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
	}

	function detectMob() {
		return ( ( window.innerWidth <= 575 ) );
	}

	var DuToanCongTrinh = function () {

		$( '.du-toan' ).on( 'click', function() {
			var $tt = $("#dtTinhThanh option:selected").val(),
			    $ln = $("#dtLoaiNha option:selected").val(),
			    $dv = $("#dtLoaiDichVu option:selected").val(),
			    $th = $("#dtHam option:selected").val();
			$( '.tt' ).html( $("#dtTinhThanh option:selected").html() )
			$( '.ln' ).html( $("#dtLoaiNha option:selected").html() )
			$( '.dv' ).html( $("#dtLoaiDichVu option:selected").html() )
			$( '.mdt' ).html( $("#dtMucDauTu option:selected").html() )
			$( '.mt' ).html( $("#dtMatTien option:selected").html() )
			$( '.gdt' ).html( formatPrice( $("#dtMucDauTu option:selected").val() ) )
			$( '.st' ).html( $("#dtSoTang").val() )

			$( '.dts' ).html( $("#dtDienTich").val() )
			$( '.dts_price' ).html( formatPrice( $("#dtDienTich").val() * $("#dtMucDauTu option:selected").val() ) )
			$( '.dts_half' ).html( $("#dtDienTich").val() / 2 )
			$( '.dts_half_price' ).html( formatPrice( $("#dtDienTich").val() * $("#dtMucDauTu option:selected").val() / 2 ) )

			$( '.tum' ).html( $("#dtTum").val() )
			$( '.tum_half' ).html( $("#dtTum").val() / 2 )
			$( '.tum_price' ).html( formatPrice( $("#dtTum").val() * $("#dtMucDauTu option:selected").val() / 2 ) )

			$( '.dtst' ).html( $("#dtSanTruoc").val() )
			$( '.dtst_half' ).html( $("#dtSanTruoc").val() / 2 )
			$( '.dtst_price' ).html( formatPrice( $("#dtSanTruoc").val() * $("#dtMucDauTu option:selected").val() / 2 ) )

			$( '.dtss' ).html( $("#dtSanSau").val() )
			$( '.dtss_half' ).html( $("#dtSanSau").val() / 2 )
			$( '.dtss_price' ).html( formatPrice( $("#dtSanSau").val() * $("#dtMucDauTu option:selected").val() / 2 ) )

			$( '.stlc' ).html( $("#dtSoBanCong").val() )
			$( '.stlc_dt' ).html( $("#dtSoBanCong").val() * $("#dtBanCong").val() )
			$( '.dts_7' ).html( $("#dtSoBanCong").val() * $("#dtBanCong").val() * 7 / 10 )
			$( '.dts_7_price' ).html( formatPrice( $("#dtSoBanCong").val() * $("#dtBanCong").val() * $("#dtMucDauTu option:selected").val() * 7 / 10 ) )

			$( '.loaimong' ).html( $( '#dtLoaiMong option:selected').html() )

				// + "&matTien=" + $("#dtMatTien").val()
				// + "&dienTichSan=" + $("#dtDienTich").val()
				// + "&soTang=" + $("#dtSoTang").val()
				// + "&dienTichTum=" + $("#dtTum").val()
				// + "&loaiMong=" + $("#dtLoaiMong").val()
				// + "&tangHam=" + $("#dtHam").val()
				// + "&dienTichSanTruoc=" + $("#dtSanTruoc").val()
				// + "&dienTichSanSau=" + $("#dtSanSau").val()
				// + "&dienTichLung=" + $("#dtLung").val()
				// + "&soLanCan=" + $("#dtSoBanCong").val()
				// + "&dienTichLanCan=" + $("#dtBanCong").val()
				// + "&dienTichLanCanThuong=" + $("#dtLanCanThuong").val()
				// + "&loaiMaiNghieng=" + $("#dtMaiNghieng").val()

				setTimeout(() => {
					let dtsd = 0;
					$( '.dtsd' ).each( function(i,e) {
						dtsd += parseInt( $(e).html() );
					} )
					$( '.dtsd_total' ).html( dtsd );

					let dttg = 0;
					$( '.dttg' ).each( function(i,e) {
						dttg += parseInt( $(e).html() );
					} )
					$( '.dttg_total' ).html( dttg );

					let pr = 0;
					$( '.pr' ).each( function(i,e) {
						pr += parseInt( $(e).html() );
					} )
					$( '.pr_total' ).html( formatPrice( pr * 1000000 ) );
				}, 0);

				$( '.math-content' ).addClass( 'show' );
			;

			$( '.table-1 .thong-bao' ).hide();
			$( '.table-1 p' ).show();
			$( '.math-content-table' ).show();

			if ( detectMob() && $tt != 0 && $ln != 0 && $dv != 0 && $th === 'not-choose' ) {
				$( '.table-1 .thong-bao' ).show();
				$( '.table-1 p' ).hide();
				$( '.math-content-table' ).hide();
			}
		} );
	}

	$( '.popup-close' ).on( 'click', function() {
		$( '.math-content' ).removeClass( 'show' );
	} )

	// Calculator
	var siteURL = $( '.caculator' ).attr( 'data-url' );
	var apiUrl = siteURL + '/wp-json/bao-gia/v1/newest?tinh-thanh=';

	async function getDataFromApi( url ) {
		await $.getJSON( url, function( data ) {
			if ( data.length !== 0 ) {
				$.each( data, function( key, val ) {
					$( '#dtLoaiDichVu' ).on( 'change', function() {
						let dv = $(this).children("option:selected").val();
						if ( "price_nctb" == dv ) {
							let _html = `
								<option value="${ val.price_nctb }">Trung bình</option>
							`;
							$( '#dtMucDauTu' ).html( _html );
						}
						else if ( "price_tct" == dv ) {
							let _html = `
								<option value="${ val.price_tct }">Trung bình</option>
							`;
							$( '#dtMucDauTu' ).html( _html );
						}
						else if ( "price_htcb" == dv ) {
							let _html = `
								<option value="${ val.price_htcb }">Trung bình</option>
							`;
							$( '#dtMucDauTu' ).html( _html );
						}
						else {
							let _html = `
								<option value="${ val.price_ckct.price_ckct_tb }">Trung bình</option>
								<option value="${ val.price_ckct.price_ckct_k }">Khá</option>
								<option value="${ val.price_ckct.price_ckct_cc }">Cao cấp</option>
							`;
							$( '#dtMucDauTu' ).html( _html );
						}
					} )
				} )
			}
		} )
	}

	$( '#dtTinhThanh' ).on( 'change', function() {
		let tinh = $(this).children("option:selected").val();
		let dataUrl = apiUrl + tinh;
		getDataFromApi( dataUrl );
	} )

	var popupForm = function() {
		setTimeout(function(){
			$( '.box-popup' ).css( 'display', 'flex' );

			$( '.box-popup i' ).on( 'click', function() {
				$('.box-popup').hide();
			} );
		}, 5000);
	}

	var scrollPopupForm = function() {
		let a = 0;
		let distanceDuanTop = $( '.section-project' ).offset().top;
		$(window).scroll( function() {
			if ( a == 0 && $(window).scrollTop() > distanceDuanTop ) {
				$( '.box-popup' ).css( 'display', 'flex' );
				a = 1;
			}
		} );
	}

	slickSlider();
	extendTinhGia();
	DuToanCongTrinh();
	if ( $( 'body' ).hasClass( 'page-template-landing' ) ) {
		countStatistic();
		calculatorMarginService();
		aos();
		popupForm();
		scrollPopupForm();
	}
} );