jQuery( function ( $ ) {
	function TraCuuHuongNha() {
		$( '.build .thar' ).on( 'click', function() {
			if ($("#tbNamSinh").val() == "") {
				$("#tbNamSinh").addClass("boxShadow");
				alert("Vui lòng nhập đầy đủ thông tin để xem");
			}
			else {
				var namSinh  = $("#tbNamSinh").val(),
					gioiTinh = $("#tbGioiTinh").val(),
					namXay   = $("#tbNamXay_XN").val();
				jQuery.ajax({
					url: ajax_object.ajax_url,
					type: "POST",
					data: {
						action: "viettel_tracuu",
						namSinh: namSinh,
						gioiTinh: gioiTinh,
						namXay: namXay,
					},
					success: function (response) {
						$("#xemHuongNha").html(response.huong_nha);
						$("#xemTuoiXayNha").html(response.tuoi_xd);
						$('.popup').removeClass('d-none');
						$('.popup-home').removeClass('d-none');

						$('.popup-close').click(function () {
							$('.popup').addClass('d-none');
							$('.popup-age').addClass('d-none');
							$('.popup-home').addClass('d-none');
						});
					},
					error: function (error) {
						loading(false);
						alert('Hệ thống đang bận, bạn vui lòng thử lại sau!');
					}
				});
			}
		} );

		$( '.xemHuongNha' ).on( 'click', function() {
			$( this ).addClass( 'active' );
			$( '.xemTuoiXayNha' ).removeClass( 'active' );
			$( '#xemHuongNha' ).show();
			$( '#xemTuoiXayNha' ).hide();
		} );
		$( '.xemTuoiXayNha' ).on( 'click', function() {
			$( this ).addClass( 'active' );
			$( '.xemHuongNha' ).removeClass( 'active' );
			$( '#xemTuoiXayNha' ).show();
			$( '#xemHuongNha' ).hide();
		} );
	}

	TraCuuHuongNha();
} );