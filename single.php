<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package viettel
 */

get_header();
?>

	<div id="primary" class="single__content container">
		<div class="breadcrumb">
			Trang chủ / <b>Tin tức</b>
		</div>
		<h1 class="section-title">Tin tức</h1>
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', get_post_type() );

		endwhile; // End of the loop.
		?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
viettel_recent_posts();
get_footer();
