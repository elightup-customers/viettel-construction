<section class="banner-top">
	<?php
	$group = rwmb_meta( 'banner-wrapper' );
	foreach ( $group as $item ) : ?>
	<div class="item">
		<?php
		$text      = $item['banner-content'];
		$image     = $item['banner-image'][0];
		$image_url = wp_get_attachment_image_src( $image, 'full', false );
		?>
		<img src="<?php echo $image_url[0]; ?>">
		<div class="item__content">
			<span><?php echo $text; ?></span>
		</div>
	</div>
	<?php endforeach; ?>
</section>
