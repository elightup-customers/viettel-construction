<?php
$shortcode       = rwmb_meta( 'form_shortcode' );
$hotline_wrapper = rwmb_meta( 'hotline' );
?>
<div class="form-ki-tuvan" data-aos="fade-down">
	<div class="form__wrapper d-flex">
		<div class="form__left">
			<h2><?php echo rwmb_meta( 'form_title' ); ?></h2>
			<?php echo do_shortcode( $shortcode ); ?>
		</div>
		<div class="form__right">
			<h3>Hotline</h3>
			<?php
			foreach ( $hotline_wrapper as $item ) :
				$image     = $item['hotline_img'][0];
				$image_url = wp_get_attachment_image_src( $image, 'full', false );

				$name  = $item['hotline_name'];
				$phone = $item['hotline_tel'];
				$email = $item['hotline_email'];
			?>
			<div class="form__right-item d-flex">
				<img src="<?php echo $image_url[0]; ?>">
				<span>
					<b><?php echo $name; ?></b>
					<p><a href="tel:<?php echo $phone; ?>">Tel: <?php echo $phone; ?></a></p>
					<p><?php echo $email; ?></p>
				</span>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>

<div class="section-video">
	<div class="section-video__wrapper">
		<iframe width="560" height="315" src="https://www.youtube.com/embed/QCcfs7rCVnc?&loop=1&playlist=QCcfs7rCVnc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	</div>
</div>
