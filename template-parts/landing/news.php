<?php
$query_args = array(
	'post_type'   => 'post',
	'post_status' => 'publish',
);
$query = new WP_Query( $query_args );

$news__imgs = rwmb_meta( 'news__left', array( 'size' => 'full' ) );
foreach ( $news__imgs as $news__img ) {
	$news__img_url = $news__img['url'];
}
?>
<div class="section-news">
	<h2 class="section-title" data-aos="fade-down"><?php echo rwmb_meta( 'news-title' ); ?></h2>
	<div class="container">
		<div class="news__wrapper d-flex">
			<div class="news__left" data-aos="fade-right">
				<a class="image_hover" href=""><img src="<?php echo $news__img_url; ?>"></a>
			</div>
			<div class="news__right" data-aos="fade-left">
				<?php while ( $query->have_posts() ) : $query->the_post();
					$title = wp_trim_words( get_the_title(), 13, esc_html__( '...', 'viettel' ) );
					?>
					<div class="news-item d-flex">
						<a href="<?php the_permalink(); ?>" class="image_hover thumbnail">
							<?php the_post_thumbnail( 'duan-thumbnail' ); ?>
						</a>
						<div class="news-item__content">
							<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
							<span class="post-excerpt">
								<?php the_excerpt(); ?>
							</span>
						</div>
					</div>
				<?php endwhile;
				wp_reset_postdata(); ?>
			</div>
		</div>
	</div>
</div>
