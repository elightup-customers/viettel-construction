<div class="math-content">
    <div class="table-1">
        <div class="popup-close"><i class="icofont icofont-close"></i></div>
        <h2>Dự toán chi phí</h2>
        <div class="thong-bao">Bạn chưa nhập đủ thông tin. Vui lòng ấn mở rộng rồi điền đầy đủ thông tin để tính giá.</div>
        <p>
        Tỉnh thành: <span class="tt"></span><br />
        Loại nhà: <span class="ln"></span><br />
        Dịch vụ xây nhà: <span class="dv"></span> <br />
        Mức đầu tư: <span class="mdt"></span><br />
        Mặt tiền: <span class="mt"></span><br />
        => Giá theo diện tích: <span class="gdt"></span> đ/m2<br /><br />
        Diện tích mặt sàn: <span class="dts"></span> m2<br />
        Số tầng: <span class="st"></span> tầng
        </p>

        <div class="math-content-table">
            <table>
                <thead>
                    <tr>
                        <td>STT</td>
                        <td>Loại</td>
                        <td>Diện tích sử dụng</td>
                        <td>Diện tích tính giá</td>
                        <td>Thành tiền</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Tầng trệt</td>
                        <td><span class="dts dtsd"></span> m2</td>
                        <td><span class="dts dttg"></span> m2</td>
                        <td><span class="dts_price pr"></span> đ</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Sân thượng (bao gồm cả Lan can)</td>
                        <td><span class="dts dtsd"></span> m2</td>
                        <td><span class="dts dttg"></span> m2</td>
                        <td><span class="dts_price pr"></span> đ</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Tum</td>
                        <td><span class="tum dtsd"></span> m2</td>
                        <td><span class="tum_half"> m2</td>
                        <td><span class="tum_price pr"> đ</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Số tầng có Lan can (trừ tầng thượng): <span class="stlc"></span></td>
                        <td><span class="stlc_dt dtsd"></span> m2</td>
                        <td><span class="dts_7 dttg"></span> m2</td>
                        <td><span class="dts_7_price pr"></span> đ</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>Loại móng: <span class="loaimong"></span></td>
                        <td><span class="dts dtsd"></span> m2</td>
                        <td><span class="dts_half dttg"></span> m2</td>
                        <td><span class="dts_half_price pr"></span> đ</td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>Tầng hầm: Có</td>
                        <td>0 m2</td>
                        <td>0 m2</td>
                        <td>0 đ</td>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td>Sân trước</td>
                        <td><span class="dtst dtsd"></span> m2</td>
                        <td><span class="dtst_half dttg"></span> m2</td>
                        <td><span class="dtst_price pr"></span> đ</td>
                    </tr>
                    <tr>
                        <td>8</td>
                        <td>Sân sau</td>
                        <td><span class="dtss dtsd"></span> m2</td>
                        <td><span class="dtss_half dttg"></span> m2</td>
                        <td><span class="dtss_price pr"></span> đ</td>
                    </tr>
                    <tr>
                        <td>9</td>
                        <td>Mái nghiêng: Mái bê tông + ngói dán</td>
                        <td>0 m2</td>
                        <td>0 m2</td>
                        <td>0 đ</td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>Tổng diện tích sử dụng:</td>
                        <td><span class="dtsd_total"></span> m2</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>Tổng diện tích tính giá:</td>
                        <td><span class="dttg_total"></span> m2</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>Tổng chi phí:</td>
                        <td><span class="pr_total"></span> VNĐ</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>