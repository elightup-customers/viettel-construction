<h2 class="section-title" data-aos="fade-down">Tính giá xây dựng</h2>
<div class="list-gdesc" data-aos="fade-down">
	<p>Quý khách vui lòng nhập đầy đủ các thông tin trong bảng tính toán, đối với trường thông tin diện tích vui lòng nhập diện tích xây dựng (không phải diện tích đất) để có kết quả tương đối chính xác.
	<br><br>
	(Ghi chú: Số liệu mang tính chất tham khảo, do đơn giá phụ thuộc vào nhiều yếu tố như điều kiện thi công, chủng loại vật liệu,….)
	</p>
</div>

<div class="tinh-toan">
	<section class="build">
		<div class="container">
			<div class="list-body d-flex">
				<div class="item" data-aos="fade-right">
					<div class="item-box d-flex">
						<h2 class="item-title">Xem hướng nhà và tuổi xây dựng</h2>
						<div class="box">
							<div class="input">
								<span>Năm sinh gia chủ:</span>
								<input id="tbNamSinh" type="text" placeholder="Vui lòng nhập thông tin...">
							</div>
							<div class="input">
								<span>Giới tính:</span>
								<select id="tbGioiTinh">
									<option value="nam">Nam</option>
									<option value="nu">Nữ</option>
								</select>
							</div>
						</div>
						<div class="box">
							<div class="input">
								<span>Năm xây dựng:</span>
								<input id="tbNamXay_XN" type="text" value="" placeholder="Vui lòng nhập thông tin...">
							</div>
							<div class="item-link viewage">
								<a href="javascript:void(0);" title="Xem ngay" class="thar btn-main">Xem ngay</a>
							</div>
						</div>
					</div>
				</div>
				<div class="build__image" data-aos="fade-left">
					<img src="<?php echo get_template_directory_uri(); ?>/images/build-image.png">
				</div>
			</div>
			<div class="popup d-none">
				<div class="popup-home d-none">
					<div class="d-flex">
						<div class="item-title xemHuongNha active">Xem hướng nhà</div>
						<div class="item-title xemTuoiXayNha">Xem tuổi xây dựng</div>
					</div>
					<div class="popup-content">
						<div class="item-body" id="xemHuongNha"></div>
						<div class="item-body d-none" id="xemTuoiXayNha"></div>
					</div>
					<div class="popup-close"><i class="icofont icofont-close"></i></div>
				</div>
			</div>
		</div>
	</section>


	<section class="caculator" data-aos="fade-down" data-url="<?php echo home_url(); ?>">
		<div class="list-body">
			<h3 class="item-title">Tính giá xây dựng</h3>
			<div class="item">
				<div class="item-body">
					<div class="input">
						<span>Chọn tỉnh/TP</span>
						<select id="dtTinhThanh">
						<option value="0">- Tỉnh thành -</option>
							<?php
							$args = [

								'post_type' => 'bao-gia',
								'orderby'   => 'name',
								'order'     => 'ASC',
								'posts_per_page' => 99,
							];

							$tinh_thanh = new WP_Query($args);
							if( $tinh_thanh->have_posts() ) :
								while( $tinh_thanh->have_posts() ) :
									$tinh_thanh->the_post();
							?>
							<option value="<?php echo rwmb_meta( 'province_id', get_the_ID() ); ?>"><?php the_title(); ?></option>
							<?php
							echo rwmb_meta( 'price_nctb', get_the_ID() );
								endwhile;
							endif;
							wp_reset_postdata();
							?>
						</select>
					</div>
					<div class="input">
						<span>Chọn loại nhà</span>
						<select id="dtLoaiNha">
							<option value="0">- Loại nhà -</option>
							<option value="600">Nhà Phố</option>
							<option value="601">Biệt Thự</option>
							<option value="602">Nhà cấp 4</option>
						</select>
					</div>
					<div class="input">
						<span>Dịch vụ xây nhà</span>
						<select id="dtLoaiDichVu">
							<option value="0">- Chọn dịch vụ -</option>
							<option value="price_nctb">Giá nhân công xây dựng trung bình</option>
							<option value="price_tct">Xây dựng phần thô</option>
							<option value="price_htcb">Xây dựng hoàn thiện cơ bản</option>
							<option value="price_ckct">Xây dựng trọn gói, chìa khóa trao tay</option>
						</select>
					</div>
					<div class="input hide-on-mobile hmb">
						<span>Mức đầu tư</span>
						<select id="dtMucDauTu">
							<option value="0">- Chọn mức đầu tư -</option>
							<!-- <option value="607">Trung bình</option>
							<option value="608">Khá</option>
							<option value="609">Cao cấp</option> -->
						</select>
					</div>
					<div class="input hide-on-mobile hmb" style="flex:0 0 12.5%;max-width:12.5%">
						<span>Mặt tiền</span>
						<select id="dtMatTien">
							<option value="0">0</option>
							<option value="610">1</option>
							<option value="611">2</option>
						</select>
					</div>
					<div class="input hide-on-mobile hmb" style="flex:0 0 12.5%;max-width:12.5%">
						<span>Diện tích mặt sàn</span>
						<input id="dtDienTich" type="number" value="0" min="0">
					</div>
					<div class="input hide-on-mobile hmb">
						<span>Số tầng</span>
						<input id="dtSoTang" style="width: calc(100% - 105px);" type="number" value="2" min="2">
					</div>
					<div class="input hide-on-mobile hmb">
						<span>Diện tích Tum</span>
						<input id="dtTum" type="number" value="0" min="0">
					</div>
					<div class="input hide-on-mobile hmb">
						<span>Loại móng</span>
						<select id="dtLoaiMong">
							<option value="50">Móng băng, móng cọc</option>
							<option value="10">Móng cốc</option>
						</select>
					</div>
				</div>
			</div>
			<div class="item" style="display: none">
				<div class="item-body">
					<div class="input">
						<span>Tầng hầm</span>
						<select id="dtHam">
							<option value="not-choose">- Chọn Tầng hầm -</option>
							<option value="0">Không có</option>
							<option value="150">Có</option>
						</select>
					</div>
					<div class="input">
						<span>Diện tích sân trước</span>
						<input id="dtSanTruoc" type="number" value="0" min="0">
					</div>
					<div class="input">
						<span>Diện tích sân sau</span>
						<input id="dtSanSau" type="number" value="0" min="0">
					</div>
					<div class="input">
						<span>Diện tích Lửng</span>
						<input id="dtLung" type="number" value="0" min="0">
					</div>
					<div class="input">
						<span>Số ban công</span>
						<input id="dtSoBanCong" type="number" value="0" min="0">
					</div>
					<div class="input">
						<span>Diện tích ban công</span>
						<input id="dtBanCong" type="number" value="0" min="0">
					</div>
					<div class="input">
						<span>Ban công tầng thượng</span>
						<input id="dtLanCanThuong" type="number" value="0" min="0">
					</div>
					<div class="input">
						<span>Mái nghiêng</span>
						<select id="dtMaiNghieng">
							<option value="0">Không có</option>
							<option value="20">Mái bê tông + Ngói dán</option>
							<option value="30">Mái tôn</option>
							<option value="70">Mái ngói + Hệ xà gồ</option>
						</select>
					</div>
				</div>
			</div>
			<div class="item-link">
				<a href="javascript:void(0);" title="Tính kết quả" class="thar du-toan btn-main">Tính kết quả</a>
				<a href="javascript:void(0);" title="" class="thar extend btn-main">Mở rộng</a>
			</div>
		</div>
	</section>
</div>
