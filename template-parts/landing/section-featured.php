<section class="section-featured">
	<div class="container">
		<?php
		$featured = rwmb_meta( 'featured__wrapper' );
		foreach ( $featured as $item ) :
			$image        = $item['featured__number'][0];
			$image_url    = wp_get_attachment_image_src( $image, 'full', false );

			$text         = $item['featured-item-left'];
			$image_slides = $item['featured-item-right'];
		?>
		<div class="section-featured__wrapper">
			<div class="section-featured__number">
				<img src="<?php echo $image_url[0]; ?>">
			</div>
			<div class="section-featured__item d-flex">
				<div class="featured__item--left" data-aos="fade-right">
					<?php echo $text; ?>
				</div>
				<div class="featured__item--right" data-aos="fade-left">
					<?php foreach( $image_slides as $image_slide ) :
						$image_slide_url = wp_get_attachment_image_src( $image_slide, 'full', false );
					?>
					<a class="">
						<img src="<?php echo $image_slide_url[0]; ?>">
					</a>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
		<?php endforeach; ?>
	</div>
</section>
