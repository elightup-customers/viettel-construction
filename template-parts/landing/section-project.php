<div class="section-project" data-aos="fade-down">
	<div class="container">
		<h2 class="section-title"><?php echo rwmb_meta( 'project-title' ); ?></h2>
		<h3 class="project__gt"><?php echo rwmb_meta( 'project-sub-title' ); ?></h3>
		<div class="projects__wrapper">
			<?php
			$project = rwmb_meta( 'projects__wrapper' );
			foreach ( $project as $item ) :
				$image     = $item['project-image'][0];
				$image_url = wp_get_attachment_image_src( $image, 'duan-thumbnail', false );

				$name      = $item['project-name'];
				$dia_diem  = $item['project-vitri'];
				$mota      = $item['project-mota'];
			?>
			<div class="project-item">
				<a href="" class="image_hover"><img src="<?php echo $image_url[0]; ?>"></a>
				<div class="project__content">
					<span class="posted-on"><?php echo $dia_diem; ?></span>
					<div class="entry-title"><?php echo $name; ?></div>
					<div class="project-mota"><?php echo $mota; ?></div>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>
