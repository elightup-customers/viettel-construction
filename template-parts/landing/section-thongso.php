<h2 class="section-title about"><?php echo rwmb_meta( 'thongso-title' ); ?></h2>
<section class="thongso" data-aos="fade-left">
	<div class="container">
		<div class="thongso__wrapper d-flex">
			<?php
			$thongso = rwmb_meta( 'thongso__wrapper' );
			foreach ( $thongso as $item ) :
				$image     = $item['thongso-image'][0];
				$image_url = wp_get_attachment_image_src( $image, 'full', false );
				$text      = $item['thongso-text'];
				$so        = $item['thongso-so'];
				$prefix    = $item['thongso-prefix'];
				$sx        = $item['thongso-option'];
			?>
			<div class="thongso__item">
				<img src="<?php echo $image_url[0]; ?>">
				<p><?php echo $text; ?></p>
				<span>
					<?php
					if ( $sx == 'default' ) : ?>
						<b class="number" data-count="<?php echo $so; ?>"><?php echo $so; ?></b>
						<?php echo $prefix; ?>
					<?php else : ?>
						<?php echo $prefix; ?>
						<b class="number" data-count="<?php echo $so; ?>"><?php echo $so; ?></b>
					<?php endif; ?>
				</span>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>
