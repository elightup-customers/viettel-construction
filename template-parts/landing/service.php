<section class="service">
	<div class="container">
		<h2 class="section-title" data-aos="fade-down"><?php echo rwmb_meta( 'service-title' ); ?></h2>
		<div class="service__content" data-aos="fade-down">
			<?php echo rwmb_meta( 'service-content' ); ?>
		</div>
		<div class="service__slider">
			<div class="slider-nav" data-aos="fade-down">
				<?php
				$slide_nav = rwmb_meta( 'slider-nav' );
				foreach ( $slide_nav as $item ) :
					$text      = $item['slider-nav-text'];
					$image     = $item['slider-nav-image'][0];
					$image_url = wp_get_attachment_image_src( $image, 'full', false );
				?>
				<div class="slider-nav__item">
					<img src="<?php echo $image_url[0]; ?>">
					<p><?php echo $text; ?></p>
				</div>
				<?php endforeach; ?>
			</div>
			<div class="slider-single">
				<?php
				$slide_single = rwmb_meta( 'slider-single' );
				foreach ( $slide_single as $item ) :
					$content   = $item['slider-single-content'];
					$image     = $item['slider-single-image'][0];
					$image_url = wp_get_attachment_image_src( $image, 'full', false );
				?>
				<div class="slider-single__item d-flex">
					<a class="image_hover" data-aos="fade-right"><img src="<?php echo $image_url[0]; ?>"></a>
					<div data-aos="fade-left"><?php echo $content; ?></div>
				</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</section>


<?php
$bg_imgs = rwmb_meta( 'background-image', array( 'size' => 'full' ) );
foreach ( $bg_imgs as $bg_img ) {
	$bg_img_url = $bg_img['url'];
}

$title     = rwmb_meta( 'background-title' );
$sub_title = rwmb_meta( 'background-subtitle' );
?>
<section class="background" style="background-image: url(<?php echo $bg_img_url; ?>);">
	<div class="container">
		<h2 class="section-title"><?php echo $title; ?></h2>
		<h3 class="section-title__sub"><?php echo $sub_title; ?></h2>
	</div>
</section>
