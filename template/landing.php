<?php
/**
 * Template Name: Landing
 * Description: Display content for the Landing page.
 */
get_header( 'landing' ); ?>

	<?php get_template_part( 'template-parts/landing/banner-top' ); ?>
	<?php get_template_part( 'template-parts/landing/service' ); ?>
	<?php get_template_part( 'template-parts/landing/section-featured' ); ?>
	<?php get_template_part( 'template-parts/landing/section-caculator' ); ?>
	<?php get_template_part( 'template-parts/landing/section-project' ); ?>
	<?php get_template_part( 'template-parts/landing/news' ); ?>
	<?php get_template_part( 'template-parts/landing/section-thongso' ); ?>
	<?php get_template_part( 'template-parts/landing/form-dki-tuvan' ); ?>

	<?php get_template_part( 'template-parts/landing/section-bangtinh' ); ?>

	<!--END-FOOTER-->
<?php get_footer( 'landing' ); ?>
